<?php
/**
 * Customization of basic functionality
 */
namespace ExtrumsSKTheme;

class CustomFunctions
{
    public function __construct()
    {
        add_action( 'after_setup_theme', array($this, 'register_navwalker'));
        register_nav_menus( array(
            'primary' => __( 'Primary Menu', 'THEMENAME' ),
        ) );
    }

    /**
     * Register Custom Navigation Walker
     */
    public function register_navwalker(){
        require_once get_template_directory() . '/inc/bootstrapNavwalker.php';
    }
}