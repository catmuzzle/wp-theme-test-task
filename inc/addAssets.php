<?php
namespace ExtrumsSKTheme;

class AddAssets
{
    public function __construct()
    {
        add_action('init', array($this, 'enqueue_boostrap_assets'));
    }

    // Boostrap scripts and styles
    public function enqueue_boostrap_assets()
    {
        wp_register_script('etsk-boostrap-scripts', get_template_directory_uri().'/scripts/bootstrap.bundle.min.js', array('jquery'), '', true);
        wp_enqueue_script('etsk-boostrap-scripts');
        wp_enqueue_style('etsk-boostrap-styles', get_template_directory_uri().'/styles/bootstrap.min.css');
    }


}