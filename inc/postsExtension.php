<?php

namespace ExtrumsSKTheme;

class PostsExtension
{

    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'etsk_textarea_meta_box'));
        add_action('save_post', array($this, 'save_etsk_field_value'));
    }

    // Add meta box to post editor
    public function etsk_textarea_meta_box() {
        add_meta_box(
            'etsk-textarea-meta-box',
            'Additional inforamtion',
            array($this, 'render_etsk_textarea_meta_box'),
            'post', // Add to 'post' post type
            'normal',
            'high'
        );
    }

    // Render content of meta box
    public function render_etsk_textarea_meta_box($post) {
        // Retrieve saved value if it exists
        $etsk_textarea_value = get_post_meta($post->ID, '_etsk_field_value', true);

        wp_editor($etsk_textarea_value, 'etsk_wysiwyg', array(
            'textarea_name' => 'etsk_wysiwyg',
            'textarea_rows' => 10,
            'teeny' => false,
        ));
    }

    // Save custom textarea value
    function save_etsk_field_value($post_id) {
        if (array_key_exists('etsk_wysiwyg', $_POST)) {
            update_post_meta($post_id, '_etsk_field_value', wp_kses_post($_POST['etsk_wysiwyg']));
        }
    }


}