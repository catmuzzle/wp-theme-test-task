<?php

namespace ExtrumsSKTheme;

class LastVisitedPost
{
    public function __construct()
    {
        add_action('template_redirect', array($this, 'store_last_visited_post_cookie'));
        add_filter( 'the_content', array($this, 'filter_post_the_content'), 1 );
    }


    public function store_last_visited_post_cookie() {
        // Get the ID of the current post being viewed
        $post_id = get_queried_object_id();
        // Check post type
        if(get_post_type($post_id) != 'post'){ return; }
        // Set the cookie with the last visited post ID
        setcookie('last_visited_post', $post_id, time() + 3600, '/');
    }



    public function filter_post_the_content( $content ) {
        //
        // Check if we're inside the single Post.
        if(!is_singular('post')){ return $content;}

        $post_additional_field = get_post_meta(get_the_ID(), '_etsk_field_value', true);
        if(!empty($post_additional_field)){
            $content .= "<div>".$post_additional_field."</div>";
        }
        if (isset($_COOKIE['last_visited_post'])) {
            $last_visitedpost_data = $this->get_post_data($_COOKIE['last_visited_post']);
            $content .= '<div class="card w-25 p-3">';
                $content.= "<span>Last visited post</span>";
                $content .= '<div class="card-header">'. $last_visitedpost_data["title"].'</div>';
                $content .= '<div class="card-body">';
                    if(!empty($last_visitedpost_data["text"])){
                        $content .= '<div class="card-text">'.$last_visitedpost_data["text"].'</div>';
                    }
                    if(!empty($last_visitedpost_data["additional_field"])){
                        $content .= '<div class="card-text">'.$last_visitedpost_data["additional_field"].'</div>';
                    }

                $content .= '</div>';
                $content .= '<a href="'.get_post_permalink($_COOKIE['last_visited_post']).'" class="btn btn-primary mt-1">'.__('read more', 'etsk').'</a>';
            $content .= '</div>';
        }

        return $content;
    }

    private function get_post_data($post_id){
        $post_data = get_post($post_id);
        if($post_data){
            return array(
                'title' => $post_data->post_title,
                'text' => (!empty($post_data->post_excerpt))? $post_data->post_excerpt : '',
                'additional_field' => get_post_meta($post_id, '_etsk_field_value', true)
            );
        }
    }
}