<?php

namespace ExtrumsSKTheme;

class PostsShortcode
{
    private $post_filed_name = '_etsk_field_value';
    public function __construct()
    {
        add_shortcode('show_post', array($this, 'show_post_shortcode'));
    }

    // Define shortcode function
    function show_post_shortcode($atts) {
        // Extract shortcode attributes
        $atts = shortcode_atts(array(
            'id' => 0,
        ), $atts, 'show_post');

        // Get post ID from shortcode attributes
        $post_id = intval($atts['id']);

        // Check if post exists
        if ($post_id && get_post_status($post_id)) {
            // Get post object
            $post = get_post($post_id);

            // Get post title
            $title = $post->post_title;

            // Get custom field value
            $custom_field_value = get_post_meta($post_id, $this->post_filed_name, true);

            if(!$custom_field_value){
                $custom_field_value = '<span style="color:#ccc;">No additional information for this post</span>';
            }

            // Build output
            ob_start(); ?>
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title"><?php echo esc_html($title); ?></h5>
                </div>
                <div class="card-body">
                    <div class="card-text"><?php echo htmlspecialchars_decode($custom_field_value); ?></div>
                </div>
            </div>
           <?php
            return ob_get_clean();;
        } else {
            return '<p>Post not found.</p>';
        }
    }

}