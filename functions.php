<?php
namespace ExtrumsSKTheme;
/**
 *  Theme functions and definitions
 */


/**
 * Autoload classes
 */
require_once get_template_directory()."/vendor/autoload.php";

//Default settings
add_theme_support( 'post-thumbnails' );
add_post_type_support( 'post', 'excerpt' );
add_theme_support( 'custom-logo' );


// Add Scripts
new AddAssets();

// Customize Default
new CustomFunctions();


// Extends default posts
new PostsExtension();



// Shortcode to displays post title and additional info filed
new PostsShortcode();



// last visited post
new LastVisitedPost();
